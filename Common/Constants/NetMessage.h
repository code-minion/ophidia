#pragma once
#include "../../RakNet/Source/MessageIdentifiers.h"

enum SnakeMessages
{
	ID_DEFAULT = ID_USER_PACKET_ENUM,
	ID_CL_DISCONNECT,		// Client reports disconnect
	ID_CL_GAME_LEAVE,		// Client reports leave game
	ID_CL_GAME_JOIN,		// Client requests join game
	ID_CL_SNAKE_MOVE,		// Snake is sending a move command
	ID_CL_STARTGAME,		// Try to start game
	ID_SV_SYNCHRONIZE_PART,	// Server sychronizing gamestates
	ID_SV_SYNCHRONIZE_FULL,	// Server sychronizing gamestates
	ID_SV_MESSAGE,			// Server sending text message
	ID_SV_INVITE,			// Server confirming join request
	ID_SV_FULL,				// Server Full
	ID_SV_INPROGRESS,		// Server's game in progress
	ID_SV_ERROR,			// Server sending text message
	ID_SV_HOST_LEFT,		// Host Quit
	ID_SV_ADVERTISE,		// Tell pingers about this great server!
	
};