#pragma once

// Unit Types
	const char UT_NOTHING	=	0;
	const char UT_WALL		=	1;
	const char UT_FOOD		=	2;
	const char UT_SEGMENT	=	3;
	const char UT_HEAD		=	4;

// Players
	const char WORLD	=	0;
	const char PLAYER01	=	1;
	const char PLAYER02	=	2;
	const char PLAYER03	=	3;
	const char PLAYER04	=	4;
	const char PLAYER05	=	5;
	const char PLAYER06	=	6;
	const char PLAYER07	=	7;
	const char PLAYER08	=	8;
	const char PLAYER09	=	9;
	const char PLAYER10	=	10;
	const char PLAYER11	=	11;
	const char PLAYER12	=	12;
	const char PLAYER13	=	13;
	const char PLAYER14	=	14;
	const char PLAYER15	=	15;
	const char PLAYER16 =	16;

// Snake Facing
	const char FACE_NONE =  0;
	const char FACE_UP	 =	1;
	const char FACE_RIGHT=	2;
	const char FACE_DOWN =	3;
	const char FACE_LEFT =	4;

// Snake Events
	const char EV_NONE	=	0;
	const char EV_GROW	=	1;
	const char EV_DIE	=	2;