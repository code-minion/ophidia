#pragma once
#include "../RakNet/Source/RakNetTypes.h"
#include <string>
#include "../../RakNet/Source/RakWString.h"

struct TPlayer
{
	RakNet::RakString	Name;
	bool				host;
	RakNet::RakNetGUID	GUID;	
		
	TPlayer()
	{		
		host = false;
	}
};

struct TSnake
{
	TPlayer				player;
	int					score;
	unsigned int		x;
	unsigned int		y;

	void init()
	{
		x = 99999;
		y = 99999;
		score = 99999;
	}
};

struct TPacketData
{
	char contents; // 0 is invalid, 1 is addserver, 2 is join lobby, 3 is snakemove
	RakNet::SystemAddress sadd;
	char x;
	char y;
};