                        ___        _     _     _ _       
                       / _ \ _ __ | |__ (_) __| (_) __ _ 
                      | | | | '_ \| '_ \| |/ _` | |/ _` |
                      | |_| | |_) | | | | | (_| | | (_| |
                       \___/| .__/|_| |_|_|\__,_|_|\__,_|
                            |_| 

Ophidia is a networked Snake Game project that is split into a server app and a client app.
It utilizes the RakNet framework which is free for poor people, students and indie developers alike.

The source, solution, project, data files and required external libraries and headers fit into this folder structure :

Ophidia 
{
	Ophidia.sln
	
	OphidiaClient
	{
	// various source
	}
	OphidiaServer
	{
	// various source
	}
	RakNet
	{
		Source
		{
		// various headers
		}
		Lib
		{
			RakNet.lib
			RakNetD.lib
		}
	}
	Runtime
	{
		RakNet.dll
		RakNetD.dll
	}
}