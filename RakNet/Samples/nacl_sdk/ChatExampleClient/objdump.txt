
chatexampleclient_x86_32_Debug.nexe:     file format elf32-nacl

Program Header:
    PHDR off    0x00000000 vaddr 0x00000000 paddr 0x00000000 align 2**2
         filesz 0x000000d4 memsz 0x000000d4 flags r--
    LOAD off    0x00002000 vaddr 0x00022000 paddr 0x00022000 align 2**16
         filesz 0x001282c9 memsz 0x001282c9 flags r-x
    LOAD off    0x00130000 vaddr 0x00150000 paddr 0x00150000 align 2**16
         filesz 0x0001dc9c memsz 0x0001dc9c flags r--
     TLS off    0x00000000 vaddr 0x00000000 paddr 0x00000000 align 2**2
         filesz 0x00000000 memsz 0x00000000 flags r--
    LOAD off    0x00150000 vaddr 0x00020000 paddr 0x00020000 align 2**16
         filesz 0x00160518 memsz 0x0016ce70 flags rw-

Dynamic Section:
  NEEDED               libpthread.so.51fe1ff9
  NEEDED               libppapi_cpp.so
  NEEDED               libstdc++.so.6
  NEEDED               libm.so.51fe1ff9
  NEEDED               libgcc_s.so.1
  NEEDED               libc.so.51fe1ff9
  INIT                 0x00022000
  FINI                 0x0014a260
  HASH                 0x00150000
  STRTAB               0x00150d90
  SYMTAB               0x00150460
  STRSZ                0x00000a68
  SYMENT               0x00000010
  DEBUG                0x00000000
  PLTGOT               0x001701dc
  PLTRELSZ             0x000003f8
  PLTREL               0x00000011
  JMPREL               0x00151a70
  REL                  0x00151a20
  RELSZ                0x00000050
  RELENT               0x00000008
  VERNEED              0x00151920
  VERNEEDNUM           0x00000005
  VERSYM               0x001517f8

Version References:
  required from libm.so.51fe1ff9:
    0x0d696910 0x00 10 GLIBC_2.0
  required from libstdc++.so.6:
    0x08922974 0x00 08 GLIBCXX_3.4
    0x056bafd3 0x00 07 CXXABI_1.3
  required from libgcc_s.so.1:
    0x0d696910 0x00 04 GLIBC_2.0
  required from libpthread.so.51fe1ff9:
    0x0d696911 0x00 09 GLIBC_2.1
    0x09691972 0x00 05 GLIBC_2.3.2
    0x0d696910 0x00 03 GLIBC_2.0
  required from libc.so.51fe1ff9:
    0x09691a73 0x00 12 GLIBC_2.2.3
    0x0d696911 0x00 11 GLIBC_2.1
    0x09691f73 0x00 06 GLIBC_2.1.3
    0x0d696910 0x00 02 GLIBC_2.0

