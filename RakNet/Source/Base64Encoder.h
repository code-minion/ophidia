#ifndef __BASE_64_ENCODER_H
#define __BASE_64_ENCODER_H

/// \brief Returns how many bytes were written.
// outputData should be at least the size of inputData * 2 + 6
int Base64Encoding(const char *inputData, int dataLength, char *outputData);

const char *Base64Map(void);

#endif
