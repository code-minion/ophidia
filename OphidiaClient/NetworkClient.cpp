#include "NetworkClient.h"
#include "BitStream.h"
#include "RakSleep.h"
#include <wchar.h>

#define SV_PORT 12082

CNetworkClient::CNetworkClient(void) : mcState(ST_NET_SEARCHING), ServerPort(SV_PORT), ClientPort(60000)
{
	mpClient = RakNet::RakPeerInterface::GetInstance();
	mpServer = 0;
	RakNet::SocketDescriptor socketDescriptor(ClientPort,0);

	// bind till we break the range!
	for (char c = 1; mpClient->Startup(1,&socketDescriptor,1) != 0; ++c)
	{
		if ( c > 100 )
		{
			mcState = ST_NET_FAILBINDS;
			return;
		}
		socketDescriptor.port = 60000 + c;
	}
	
}



CNetworkClient::~CNetworkClient(void)
{

}

void CNetworkClient::Broadcast()
{
	mpClient->Ping("255.255.255.255", 12082, true);
}

bool CNetworkClient::ProcessPackets()
{
	RakNet::Packet* p;

	for (p=mpClient->Receive();p;mpClient->DeallocatePacket(p),p=mpClient->Receive())
	{
		unsigned char data = p->data[0];
		switch(data)
		{
			case ID_CONNECTION_REQUEST_ACCEPTED:
				mcState = ST_NET_CONNECTED;
				mpServer = &p->guid;
				break;
			case ID_DISCONNECTION_NOTIFICATION:

				mpServer = 0;
				break;
			case ID_SV_ADVERTISE:
				{// only occurs when a Server is responding to Broadcast!
					//if (FindServer(p->guid) == -1)
					
					if (p->bitSize > 39)
					{
						RakNet::BitStream bsIn(p->data,p->length,false);
						RakNet::RakWString name;
						bsIn.IgnoreBytes(1);
						bsIn.Read(name);
						unsigned char numPlayers = p->data[11];
						return true;
					}					
				}	
				break;
			case ID_UNCONNECTED_PONG:	
				if (mcState == ST_NET_SEARCHING)
				{
					RakNet::BitStream bsIn(p->data,p->length,false);
					bsIn.IgnoreBytes(1);
					RakNet::RakString rs;
					bsIn.Read(rs);
					if (rs == "KitSnakes");
						mpClient->Connect(p->systemAddress.ToString(false),p->systemAddress.GetPort(),0,0,0);
				}
					break;
			default:
				{
				}
		}
	}
	if (p == 0)
	{
		RakSleep(30);
		return false;
	}
	return false;	
}

//char CNetworkClient::FindServer(RakNet::RakNetGUID guid)
//{
//	for (unsigned char c = 0; c < mpServerList.size(); ++c)
//	{
//		if (mpServerList[c]->Guid.g == guid.g)
//		{// these gooids, they alike
//			return c;
//		}
//	}
//	return -1;
//}

void CNetworkClient::JoinServer(RakNet::RakNetGUID guid, RakNet::RakString name)
{
	//char index = FindServer(guid);
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_CL_GAME_JOIN);
	//wcstombs(temp,name,name.GetLength());
	//char test[] = "lol hi";
	//bsOut.Write(name,(size_t)5);
	bsOut.Write(name);

	// The Packet Data Should Look Like : [id 1] [name 32] so the packet data is 33 bytes
	mpClient->Send(&bsOut,MEDIUM_PRIORITY,RELIABLE_ORDERED,0,guid,false);
}