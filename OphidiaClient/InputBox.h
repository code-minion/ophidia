#pragma once
#include "../RakNet/Source/RakString.h"
#include <Windows.h>

#define MAX_CHAR_INPUT 32

class CInputBox
{
protected:
	wchar_t*		mpBuffer;
	unsigned int	m_uBufferSize;	//!< The total size of the buffer.
	unsigned int	m_uBufferPos;	//!< The position of the next char in the buffer to be entered by the user.
	bool			mbActive;
	POINT			mtLocation;
	unsigned int	mPlusSize;
	HDC				hBBDC;
public:
	CInputBox(void);
	~CInputBox(void);
		
	void SetBuffers(HDC _bbdc) { hBBDC = _bbdc; };
	RakNet::RakString GetString();
	//inline void SetString(wchar_t* name) { mpBuffer = name; };
	inline bool ToggleActive() { return mbActive = !mbActive; };
	char Update( TCHAR input);//, bool ShiftDown );
	void Draw(HWND hWnd);
};

