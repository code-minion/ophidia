#pragma once
#include "../Common/Constants/Const.h"
#include "../Common/Constants/Structs.h"
#include "Timer.h"
#include "MainMenu.h"
#include "ConnectionScreen.h"
#include "NetworkClient.h"
#include <Windows.h>
#include <vector>
#include "DS_Queue.h"
#include "GameScreen.h"

// Game States
	const char ST_MAIN	=	0; // main menu
	const char ST_CONN	=	1; // connection screen
	const char ST_LOBBY	=	2; // game lobby screen
	const char ST_GAME	=	3; // game running state
	const char ST_SCORE	=	4; // highscore screen

class Win32App
{
public:
	Win32App(HINSTANCE hInstance);
	Win32App();
	virtual ~Win32App();

	HINSTANCE getAppInst();
	HWND      getMainWnd();
	void resetOMTargetsAndViewport();
	
	void ProcessClick(LPARAM lParam);

	int run();

	// Framework methods.  Derived client class overrides these methods to 
	// implement specific application requirements.

	virtual void initApp(HINSTANCE hInstance);
	virtual void onResize();// reset projection/etc
	virtual void updateScene(float dt);
	virtual void drawScene(); 
	void StateManager();
	virtual LRESULT msgProc(UINT msg, WPARAM wParam, LPARAM lParam);

protected:
	void initMainWindow();
	
private:
protected:

	HINSTANCE mhAppInst;
	HWND      mhMainWnd;
	bool      mAppPaused;
	bool      mMinimized;
	bool      mMaximized;
	bool      mResizing;

	Timer mTimer;

	std::wstring mFrameStats;
 

	// Derived class should set these in derived constructor to customize starting values.
	std::wstring			mMainWndCaption;
	int						mClientWidth;
	int						mClientHeight;
	char					mcState;
	HDC						hFBDC;
	HDC						hBBDC;
	HBITMAP					hBitmap;
	HBITMAP					hBitmapOld;	
	CMainMenu*				mpMainMenu;
	std::vector<TSnake*>	mpPlayer;
	CNetworkClient*			mpNetClient;
	CConnectionScreen*		mpConScreen;
	RakNet::RakNetGUID		mGuid;
	CGameScreen*			mpGameScreen;
};
