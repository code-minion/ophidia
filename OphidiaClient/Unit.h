#pragma once
#include <Windows.h>
#include "../Common/Constants/Const.h"

class CUnit
{
public:
	CUnit(void);
	virtual ~CUnit(void);
	char GetUnitType() { return mcType; };
	char GetUnitOwner() { return mcOwner; };
	const POINT& GetPos() { return mtPosition; };

protected:
	POINT	mtPosition;
	char	mcType;
	char	mcOwner;
};

