#include "Snake.h"

CSnake::CSnake() : mcOwner(1), mcFacing(1)
{
}

CSnake::CSnake(char _owner, char _facing = 1) : mcOwner(_owner), mcFacing(_facing)
{
}

CSnake::~CSnake(void)
{
}

bool CSnake::Move(char _dir)
{
	bool success = false;

	POINT tHeadPos = Segments[0].GetPos();
	POINT tSeg1Pos = Segments[1].GetPos();

	switch ( _dir )
	{
	case FACE_NONE:
		// no movement input? maintain course!		
		break;
	case FACE_UP:
		{
			if (tSeg1Pos.y < tHeadPos.y)
			{
				// invalid movement - ignore input
				break;
			}
			mcFacing = FACE_UP;
			success = true;
			break;
		}
	case FACE_RIGHT:
		{
			if (tSeg1Pos.x > tHeadPos.x)
			{
				// invalid movement - ignore input
				break;
			}
			mcFacing = FACE_RIGHT;
			success = true;
			break;
		}
	case FACE_DOWN:
		{
			if (tSeg1Pos.y > tHeadPos.y)
			{
				// invalid movement - ignore input
				break;
			}
			mcFacing = FACE_DOWN;
			success = true;
			break;
		}
	case FACE_LEFT:
		{
			if (tSeg1Pos.x > tHeadPos.x)
			{
				// invalid movement - ignore input
				break;
			}
			mcFacing = FACE_LEFT;
			success = true;
			break;
		}
	}

	return success;
}

void CSnake::Collide(CUnit& _unit)
{
	// if we collided with player 0 (World)
	if(_unit.GetUnitOwner() == WORLD)
	{
		char who = -1;
		// check for object type
		switch ( who = _unit.GetUnitType() )
		{
		case UT_FOOD :
			{
				// we ate FOOD! This is a good thing!
				// if FOOD is a Powerup
				//	run Powerup script!
				// else
				//	Grow()
			}
		case UT_WALL :
			{
				// Head collided with wall at high speeds
				// brain is now mush
				// Die(WORLD)
			}
		}
	}
	else // we collided with a player
	{
		// This player has killed us
		// Die(who);
	}
}


void CSnake::Grow()
{

	if (!tEvent)
	{
		tEvent = new TEvent;
	}

	tEvent->cType = EV_GROW;
}

void CSnake::Die(char player)
{
	if (!tEvent)
	{
		tEvent = new TEvent;
	}

	tEvent->cType = EV_DIE;
	tEvent->cCulprit = player;
}