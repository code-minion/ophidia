#include "ConnectionScreen.h"


CConnectionScreen::CConnectionScreen() : mcState(0)
{	
	mRECT.top = 0;
	mRECT.left = 0;
	mRECT.right = 800;
	mRECT.bottom = 600;
}

void CConnectionScreen::Init(HWND hWnd,HDC hDc)
{
	mhWnd = hWnd;
	hBBDC = hDc;
}

void CConnectionScreen::AddButton(long x1, long y1, long x2, long y2, unsigned long color, wchar_t str[256])
{
	CMenuItem* pTemp = new CMenuItem(x1,y1,x2,y2,color,str);
	Buttons.push_back(pTemp);
}

void CConnectionScreen::AddButton(long x1, long y1, long x2, long y2, unsigned long color, RakNet::Packet* pack)
{
	char* dest = 0;
	wchar_t test[256];
	const size_t newsize = 200;
	pack->systemAddress.ToString(true, dest);
	wsprintf(test, L"", L"%s", dest);

	CMenuItem* pTemp = new CMenuItem(x1,y1,x2,y2,color,test);
	Buttons.push_back(pTemp);
}

CConnectionScreen::~CConnectionScreen(void)
{
}

void CConnectionScreen::Draw()
{
	FillRect(hBBDC, &mRECT, (HBRUSH)GetStockObject(LTGRAY_BRUSH));	// Clear back buffer.

	for (unsigned short s = 0; s < Buttons.size(); ++s)
	{
		Buttons[s]->Draw(mhWnd);
	}
	
	wchar_t nameMsg[] = L"Searching For Servers";

	TextOutW(hBBDC, 200, 100, nameMsg, wcslen(nameMsg));
	const char* temp;
	for (unsigned short s = 0; s < mpServerList.size(); ++s)
	{
		temp = mpServerList[s]->Name.C_String();
		TextOutA(hBBDC, 200, 100, temp, strlen(temp));;
	}
}

void CConnectionScreen::Update()
{

}