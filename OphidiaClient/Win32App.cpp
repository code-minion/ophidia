//#include "stdafx.h"
//#include <Windows.h>
#include "Win32App.h"
#include <sstream>

LRESULT CALLBACK
MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

	static Win32App* app = 0;
	TCHAR wc = 0;

	if ( msg == WM_CREATE )
	{
		CREATESTRUCT *pCreate = reinterpret_cast<CREATESTRUCT*>(lParam);
        app = reinterpret_cast<Win32App*>(pCreate->lpCreateParams);
        SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)app);
	}
    else
    {
        app = reinterpret_cast<Win32App*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));
    }
	
	// Don't start processing messages until after WM_CREATE.
	if( app )
		return app->msgProc(msg, wParam, lParam);
	else
		return DefWindowProc(hwnd, msg, wParam, lParam);
}

Win32App::Win32App(HINSTANCE hInstance) : mcState(ST_MAIN)
{
}

Win32App::Win32App() : mcState(ST_MAIN)
{
	mhMainWnd   = 0;
	mAppPaused  = false;
	mMinimized  = false;
	mMaximized  = false;
	mResizing   = false;

	mMainWndCaption = L"Ophidia Client";
	mClientWidth    = 600;
	mClientHeight   = 600;
	mpMainMenu = new CMainMenu(mClientWidth,mClientHeight);
	mpConScreen = new CConnectionScreen();
	mpNetClient = new CNetworkClient();
	mpGameScreen = new CGameScreen();
	mpNetClient->Broadcast();
	//mpConScreen->mpServerList = &mpNetClient->mpServerList;
}

Win32App::~Win32App()
{	
	// At destruction of program.
	ReleaseDC(mhMainWnd,hBBDC);
	DeleteDC(hBBDC);

	ReleaseDC(mhMainWnd,hFBDC);
}

HINSTANCE Win32App::getAppInst()
{
	return mhAppInst;
}

HWND Win32App::getMainWnd()
{
	return mhMainWnd;
}
void Win32App::resetOMTargetsAndViewport()
{
}

int Win32App::run()
{
	MSG msg = {0};
 
	mTimer.reset();

	while(msg.message != WM_QUIT)
	{
		// If there are Window messages then process them.
		if(PeekMessage( &msg, 0, 0, 0, PM_REMOVE ))
		{
            TranslateMessage( &msg );
            DispatchMessage( &msg );
		}
		// Otherwise, do animation/game stuff.
		else
        {	
			mTimer.tick();

			if( !mAppPaused )
				updateScene(mTimer.getDeltaTime());	
			else
				Sleep(50);

			drawScene();
        }
    }
	return (int)msg.wParam;
}

void Win32App::initApp(HINSTANCE hInstance)
{
	mhAppInst = hInstance;
	initMainWindow();
}
 
void Win32App::onResize()
{

}

void Win32App::StateManager()
{	
	switch (mcState)
	{
	case ST_MAIN:
		// check if we are still on main
		if (mpMainMenu->NameSet())
		{
			TSnake* temp = new TSnake;	
			temp->player.Name = mpMainMenu->GetName();
			mpPlayer.push_back(temp);
			mcState = ST_CONN;
		}
		break;
	case ST_CONN:
		// check if game started or user cancelled
		{			
			RakNet::SystemAddress sadd;
			if (mpNetClient->ProcessPackets())
			{				
			}

			if (mpNetClient->mcState == ST_NET_SEARCHING)
				mpNetClient->Broadcast();

			if ( mpNetClient->mcState == ST_NET_CONNECTED )
			{
				mpNetClient->JoinServer(*mpNetClient->GetServer(), mpPlayer[0]->player.Name);
			}
		}
		break;
	default:
		//invalid state
		break;

	}
}

void Win32App::updateScene(float dt)
{

	StateManager();

	// Code computes the average frames per second, and also the 
	// average time it takes to render one frame.
	static int frameCnt = 0;
	static float t_base = 0.0f;

	frameCnt++;

	// Compute averages over one second period.
	if( (mTimer.getGameTime() - t_base) >= 1.0f )
	{
		float fps = (float)frameCnt; // fps = frameCnt / 1
		float mspf = 1000.0f / fps;

		std::wostringstream outs;   
		outs.precision(6);
		outs << L"FPS: " << fps << L"\n" 
			 << "Milliseconds: Per Frame: " << mspf;
		mFrameStats = outs.str();
		
		// Reset for next average.
		frameCnt = 0;
		t_base  += 1.0f;
	}
}

void Win32App::drawScene()
{
	switch (mcState)
	{
	case ST_MAIN:
		mpMainMenu->Draw();
		break;
	case ST_CONN:
		mpConScreen->Draw();
		break;
	case ST_GAME:
		{
			mpGameScreen->Draw();
		}
		break;
	}
	// At end of Draw().
	BitBlt(hFBDC, 0, 0, 600, 600, hBBDC, 0, 0, SRCCOPY);

}

LRESULT Win32App::msgProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch( msg )
	{
	// WM_ACTIVATE is sent when the window is activated or deactivated.  
	// We pause the game when the window is deactivated and unpause it 
	// when it becomes active.  
	case WM_ACTIVATE:
		if( LOWORD(wParam) == WA_INACTIVE )
		{
			mAppPaused = true;
			mTimer.stop();
		}
		else
		{
			mAppPaused = false;
			mTimer.start();
		}
		return 0;

		
		// KeyboardCode
		case WM_KEYDOWN:
		{
			// shift is held
			if (wParam == VK_SHIFT)
			break;
		}
		case WM_KEYUP:
		{
			// shift was let go
			if (wParam == VK_SHIFT)
			break;
		}
		//case WM_CHAR:
		//{
		//	TCHAR wc = (TCHAR)wParam;
		//	pMainMenu->UpdateInputBox(wc);
		//	break;
		//}
		case WM_LBUTTONUP: // Left mouse button clicked
		{
			ProcessClick(lParam);
		}

	// WM_SIZE is sent when the user resizes the window.  
	case WM_SIZE:
		// Save the new client area dimensions.
		mClientWidth  = LOWORD(lParam);
		mClientHeight = HIWORD(lParam);
		
		return 0;

	// WM_EXITSIZEMOVE is sent when the user grabs the resize bars.
	case WM_ENTERSIZEMOVE:
		mAppPaused = true;
		mResizing  = true;
		mTimer.stop();
		return 0;

	// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
	// Here we reset everything based on the new window dimensions.
	case WM_EXITSIZEMOVE:
		mAppPaused = false;
		mResizing  = false;
		mTimer.start();
		onResize();
		return 0;
 
	// WM_DESTROY is sent when the window is being destroyed.
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	// The WM_MENUCHAR message is sent when a menu is active and the user presses 
	// a key that does not correspond to any mnemonic or accelerator key. 
	case WM_MENUCHAR:
        // Don't beep when we alt-enter.
        return MAKELRESULT(0, MNC_CLOSE);

	// Catch this message so to prevent the window from becoming too small.
	case WM_GETMINMAXINFO:
		((MINMAXINFO*)lParam)->ptMinTrackSize.x = 200;
		((MINMAXINFO*)lParam)->ptMinTrackSize.y = 200; 
		return 0;
	}

	return DefWindowProc(mhMainWnd, msg, wParam, lParam);
}


void Win32App::initMainWindow()
{
	WNDCLASS wc;
	wc.style         = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc   = MainWndProc; 
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = mhAppInst;
	wc.hIcon         = LoadIcon(0, IDI_APPLICATION);
	wc.hCursor       = LoadCursor(0, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName  = 0;
	wc.lpszClassName = L"GDIWndClassName";

	if( !RegisterClass(&wc) )
	{
		MessageBox(0, L"RegisterClass FAILED", 0, 0);
		PostQuitMessage(0);
	}

	// Compute window rectangle dimensions based on requested client area dimensions.
	RECT R = { 0, 0, mClientWidth, mClientHeight };
    AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);
	int width  = R.right - R.left;
	int height = R.bottom - R.top;

	mhMainWnd = CreateWindow(L"GDIWndClassName", mMainWndCaption.c_str(), 
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, width, height, 0, 0, mhAppInst, this); 
	if( !mhMainWnd )
	{
		MessageBox(0, L"CreateWindow FAILED", 0, 0);
		PostQuitMessage(0);
	}
	mpMainMenu->SetHwnd(mhMainWnd);

	// making some DevCons:
	hFBDC = GetDC(mhMainWnd);
	hBBDC = CreateCompatibleDC(hFBDC);

	// Make text BG transparent
	SetBkMode(hBBDC, TRANSPARENT);

	// Setup bitmaps so BB actually works
	hBitmap = CreateCompatibleBitmap(hFBDC, width, height);

	// used in dtor
	hBitmapOld = (HBITMAP)SelectObject(hBBDC, hBitmap);

	// pass the backbuffer down the hierarchy
	mpMainMenu->SetBuffers(hBBDC);
	mpConScreen->Init(mhMainWnd,hBBDC);

	ShowWindow(mhMainWnd, SW_SHOW);
	//UpdateWindow(mhMainWnd);
}

void Win32App::ProcessClick(LPARAM lParam)
{
	float fX = LOWORD(lParam);
	float fY = HIWORD(lParam);

	char button = mpMainMenu->CheckButtonHit(fX, fY);

	switch (button) 
	{
	case -1:
		// yeah, that's not a button.
		break;
	case 0:
	default:
		break;
	}
}