#include "InputBox.h"

CInputBox::CInputBox(void) : m_uBufferPos(0), m_uBufferSize(MAX_CHAR_INPUT), mPlusSize(220)
{
	mpBuffer = new wchar_t [MAX_CHAR_INPUT];
	memset(mpBuffer,0,sizeof(mpBuffer));
	mtLocation.x = 210;
	mtLocation.y = 300;
	mbActive = true;
}

RakNet::RakString CInputBox::GetString()	
{ 
	RakNet::RakString retval;
	char var[32];
	size_t tee = 0;
	wcstombs_s(&tee,var,mpBuffer,wcslen(mpBuffer));
	retval = var;
	return retval; 
};

CInputBox::~CInputBox(void)
{
}

char CInputBox::Update( TCHAR input)//, bool ShiftDown )
{
	if (mbActive)
	{
		if (m_uBufferPos >= 20)
		{
			mtLocation.x = 210 - (4 * (m_uBufferPos - 20 + 1));
			mPlusSize = 220 + (8 * (m_uBufferPos - 20 + 1));
		}
		// if input box is active
		if (input == '\t')			
			return -1;

		if (input == '\b')
		{
			//this is a backspace
			if (m_uBufferPos > 0)
				m_uBufferPos--;
			mpBuffer[m_uBufferPos] = 0;
			return 0;
		}

		if ( input == '\n' || input == '\r' )
		{
		// confirm entry!
			return 1;
		}

		//int shiftcase = 0;
		//if(ShiftDown)
		//{
		//	shiftcase = -32;
		//}
		
		if ( m_uBufferSize > m_uBufferPos+1 )
		{
			mpBuffer[m_uBufferPos] = input; 
			m_uBufferPos++;
			mpBuffer[m_uBufferPos] = 0;
		}


	}

	return false; // no newline found.
}

void CInputBox::Draw(HWND hWnd)
{
	RECT r;
	SetRect(&r, mtLocation.x - 25 , mtLocation.y -10 , mtLocation.x + mPlusSize, mtLocation.y +25);

	FillRect(hBBDC, &r, (HBRUSH)GetStockObject(mbActive?WHITE_BRUSH:LTGRAY_BRUSH));
	TextOutW(hBBDC, mtLocation.x, mtLocation.y, mpBuffer, wcslen(mpBuffer));
}