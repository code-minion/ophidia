#include "MainMenu.h"
//
//unsigned long RGBA2DWORD(int iR, int iG, int iB, int iA)
//{
//	return ((iA << 24) | (iR << 16) | (iG << 8) | iB);
//}

CMainMenu::CMainMenu(int _width, int _height) : _nameSet(false)
{
	mpInputBox = new CInputBox();
	mpTitleText = new wchar_t[256];
	//AddButton(100,100,200,150, RGB(255,0,0), L"I'm a button!"); this was for testing
	mRECT.bottom	= 0;
	mRECT.top		= 0;
	mRECT.right		= _width;
	mRECT.bottom	= _height;
	mhFonticus = CreateFont(50,0,45,0,FW_BOLD,0,0,0,0,0,0,5,0,L"SYSTEM_FIXED_FONT");
	mhPen = CreatePen(PS_DASHDOT, 1, RGB(20,40,80));

}


CMainMenu::~CMainMenu(void)
{
	DeleteObject(mhFonticus);
	DeleteObject(mhPen);
}

char CMainMenu::CheckButtonHit(float fX, float fY)
{
	for (unsigned int i = 0; i < Buttons.size(); ++i)
	{
		// Did I click a button?
		if ((Buttons[i]->TopLeft.x < fX)&&(Buttons[i]->BottomRight.x > fX))
			if ((Buttons[i]->TopLeft.y < fY)&&(Buttons[i]->BottomRight.y > fY))
				// Yes I did!
				return Buttons[i]->cID;
	}
	// no buttons hit
	return -1;
}

void CMainMenu::AddButton(long x1, long y1, long x2, long y2, unsigned long color, wchar_t str[256])
{
	CMenuItem* pTemp = new CMenuItem(x1,y1,x2,y2,color,str);
	Buttons.push_back(pTemp);
}

void CMainMenu::UpdateInputBox(TCHAR msg)
{
	char retval = mpInputBox->Update(msg);

	// value of 1 means 'enter'!
	if (retval == 1)
	{
		// if there is at least 1 character
		if (strlen(mpInputBox->GetString()) > 0)
		{
			_nameSet = true;
			//mpInputBox->ToggleActive();
		}
	}
}

void CMainMenu::Draw()
{
	FillRect(hBBDC, &mRECT, (HBRUSH)GetStockObject(LTGRAY_BRUSH));	// Clear back buffer.

	for (unsigned short s = 0; s < Buttons.size(); ++s)
	{
		Buttons[s]->Draw(mhWnd);
	}
	mpInputBox->Draw(mhWnd);
		
	wchar_t nameMsg[] = L"What name do you wish to use?";

	TextOutW(hBBDC, 200, 250, nameMsg, wcslen(nameMsg));

	HFONT OldFont = (HFONT)SelectObject(hBBDC, mhFonticus);
	//HPEN OldPen = (HPEN)SelectObject(hBBDC, mhPen);

	mpTitleText = L"OPHIDIA";

	COLORREF OldColor = SetTextColor(hBBDC, RGB(20,40,80));
	TextOutW(hBBDC, 200, 150, mpTitleText, wcslen(mpTitleText));
	
	SelectObject(hBBDC, OldFont);
	//SelectObject(hBBDC, OldPen);	
	SetTextColor(hBBDC, OldColor);

	OldFont = 0;
	//OldPen = 0;
}

void CMainMenu::SetBuffers(HDC _bbdc) 
{
	hBBDC = _bbdc; 

	mpInputBox->SetBuffers(_bbdc);  	

	for (unsigned short s = 0; s < Buttons.size(); ++s)
	{
		Buttons[s]->SetBuffer(_bbdc);
	}
};