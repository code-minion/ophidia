#pragma once
#include <vector>
#include "../Common/Constants/Structs.h"
#include "../Common/Constants/Const.h"

class CGameScreen
{

public:
	CGameScreen(void);
	~CGameScreen(void);

	void Draw();
	void InitSnakes(std::vector<TSnake*>& TS);
	void Update(std::vector<TSnake*>& TS);
};

