#pragma once
#include "../Common/Constants/NetMessage.h"
#include "../Common/Constants/Structs.h"
#include "../Common/Constants/Const.h"
#include "RakPeerInterface.h"
#include "RakString.h"
#include <vector>
#include "DS_Queue.h"

const char ST_NET_SEARCHING = 0;	// state of searching for more servers to enumerate
const char ST_NET_CONNECTED	= 1;	// fully connected! 
const char ST_NET_FAILBINDS	= 2;	// failed to bind entire port range to socket! EPIC FAIL!
const char ST_NET_LOBBY		= 3;
const char ST_NET_GAME		= 4;

struct TServer
{
	RakNet::SystemAddress	SystemAddress;
	char					NumPlayers;
	RakNet::RakNetGUID		Guid;
	RakNet::RakString		Name;
};

class CNetworkClient
{
public:	
	//std::vector<TServer*>					mpServerList;
	DataStructures::Queue<RakNet::Packet*>*	mpQ;
	char									mcState;

private:
	RakNet::RakPeerInterface*				mpClient;
	RakNet::RakNetGUID*						mpServer;
	unsigned int							ClientPort;
	unsigned int							ServerPort;
public:
	CNetworkClient(void);
	~CNetworkClient(void);
	void Broadcast();
	bool ProcessPackets();
	//char FindServer(RakNet::RakNetGUID guid);
	RakNet::RakNetGUID* GetServer() { return mpServer; };
	void JoinServer(RakNet::RakNetGUID guid, RakNet::RakString name);
};

