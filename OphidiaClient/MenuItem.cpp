#include "MenuItem.h"


CMenuItem::CMenuItem(void)
{
}

CMenuItem::CMenuItem(long x1, long y1, long x2, long y2, COLORREF color, wchar_t str[256]) : cID(0)
{
	TopLeft.x = x1;
	TopLeft.y = y1;
	BottomRight.x = x2;
	BottomRight.y = y2;
	mColor = CreateSolidBrush(color);
	mText = str;
}

CMenuItem::~CMenuItem(void)
{
	DeleteObject(mColor);
}

void CMenuItem::Initialize(long x1, long y1, long x2, long y2, unsigned long color, wchar_t str[256])
{
	TopLeft.x = x1;
	TopLeft.y = y1;
	BottomRight.x = x2;
	BottomRight.y = y2;
	mColor = CreateSolidBrush(color);
	mText = str;
}

void CMenuItem::Draw(HWND hWnd)
{
	RECT r;
	SetRect(&r, TopLeft.x , TopLeft.y , BottomRight.x , BottomRight.y );
	SelectObject(hBBDC, mColor);
	FillRect(hBBDC, &r, mColor);
	TextOutW(hBBDC, TopLeft.x + 10 , TopLeft.y + 17 , mText, wcslen(mText));
}
