#pragma once
#include <Windows.h>

class CMenuItem
{
public:
	POINT			TopLeft;
	POINT			BottomRight;
	HBRUSH			mColor;
	//unsigned long	mColor;
	wchar_t*		mText;
	char			cID;
	HDC				hBBDC;
public:
	CMenuItem(void);
	CMenuItem(long x1, long y1, long x2, long y2, unsigned long color, wchar_t str[256]);
	~CMenuItem(void);
	void Initialize(long x1, long y1, long x2, long y2, unsigned long color, wchar_t str[256]);
	void Draw(HWND hWnd);
	void SetBuffer(HDC _hbbdc) { hBBDC = _hbbdc; };
	//POINT GetTopLeft() { return TopLeft; };
	//POINT GetBottomRight() { return BottomRight; };
};

