#pragma once

#include "InputBox.h"
#include "MenuItem.h"
#include <vector>

// Takes in a name and connects directly to server, that's it
class CMainMenu
{
public:
private:
	wchar_t*				mpTitleText;
	std::vector<CMenuItem*>	Buttons;
	CInputBox*				mpInputBox;
	HWND					mhWnd;
	RECT					mRECT;	
	HDC						hBBDC;
	HPEN					mhPen;
	HFONT					mhFonticus;
	bool					_nameSet;

public:
	CMainMenu(int _width, int _height);
	~CMainMenu(void);

	RakNet::RakString GetName()	{ return mpInputBox->GetString(); };
	bool& NameSet() { return _nameSet; };
	void SetHwnd(HWND hwnd) { mhWnd = hwnd; };
	void SetBuffers(HDC _bbdc);
	void Draw();
	char CheckButtonHit(float fX, float fY);
	void UpdateInputBox(TCHAR msg);
	void AddButton(long x1, long y1, long x2, long y2, unsigned long color, wchar_t str[256]);
};
