#pragma once
#include "NetworkClient.h"
#include "MenuItem.h"
#include <vector>

const char ST_CON_CANCELLED = -1; // cancelled, return to main menu
const char ST_CON_SEARCHING = 0;  // broadcasting ping, looking for servers
const char ST_CON_CONNECTED = 1;  // request to join accepted! move to LOBBY state!

class CConnectionScreen
{
public:
	std::vector<TServer*>					mpServerList;

private:	
	HWND									mhWnd;
	char									mcState;
	HDC										hBBDC;
	std::vector<CMenuItem*>					Buttons;
	RECT									mRECT;

public:
	CConnectionScreen();
	void Init(HWND,HDC);
	void AddButton(long x1, long y1, long x2, long y2, unsigned long color, wchar_t str[256]);
	void AddButton(long x1, long y1, long x2, long y2, unsigned long color, RakNet::Packet* pack);
	~CConnectionScreen(void);
	void Draw();
	void Update();
	char& GetState() { return mcState; };
	
	void Reactivate() { mcState = ST_CON_SEARCHING; };
};

