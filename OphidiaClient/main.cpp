#include "Win32App.h"
#include <deque>

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

class OphidiaClient : public Win32App
{
public:
	OphidiaClient(HINSTANCE hInstance);
	OphidiaClient();
	~OphidiaClient();

	void initApp(HINSTANCE hInstance);
	void onResize();
	void updateScene(float dt);
	void drawScene(); 

	LRESULT msgProc(UINT msg, WPARAM wParam, LPARAM lParam);

private:
	POINT mOldMousePos;
};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE _hPrevInstance, PSTR _pstrCmdLine, int iCommands)
{
		// Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif
	
	OphidiaClient theApp(hInstance);

	theApp.initApp(hInstance);

	return theApp.run();
}


OphidiaClient::OphidiaClient(HINSTANCE hInstance)
{
	POINT mOldMousePos = { 0 };
}

//OphidiaClient::OphidiaClient()
//{
//}

OphidiaClient::~OphidiaClient()
{
}

void OphidiaClient::initApp(HINSTANCE hInstance)
{
	Win32App::initApp(hInstance);
}

void OphidiaClient::onResize()
{
}

void OphidiaClient::updateScene(float dt)
{
	Win32App::updateScene(dt);
}

void OphidiaClient::drawScene()
{
	Win32App::drawScene();
}

LRESULT OphidiaClient::msgProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
		case WM_CHAR:
		{
			TCHAR wc = (TCHAR)wParam;
			mpMainMenu->UpdateInputBox(wc);
			break;
		}
	}
	return Win32App::msgProc(msg,wParam,lParam);
}