#pragma once
#include <deque>
#include "../Common/Constants/Structs.h"
#include "Unit.h"
#include "../Common/Constants/Const.h"

struct TEvent
{
	char cType;
	char cCulprit;
};

class CSnake
{
public:
	CSnake();
	CSnake(char _owner, char _facing);
	~CSnake(void);	
	bool Move(char _direction);
	void Collide(CUnit& _unit);
	void Grow();
	void Die(char player);
	void Update();
	TPlayer* GetPlayer() { return player; };

protected:

private:

public:	
	std::deque<CUnit> Segments;

protected:
	
private:
	char mcFacing;
	char mcOwner;
	TEvent* tEvent;
	TPlayer* player;
};

