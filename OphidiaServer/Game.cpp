#include "Game.h"
#include "RakPeerInterface.h"

CGame::CGame(void) : cState(0), maxSize(16)
{
	atSnakes.reserve(16);
}


CGame::~CGame(void)
{
}

void CGame::InsPlayer(TSnake& _snake)
{
	atSnakes.push_back(_snake);
	return;
}

void CGame::InsPlayer(RakNet::RakString name, RakNet::SystemAddress sadd, bool Host)
{
	//RakNet::RakPeerInterface* tool = 0;
	TSnake temp;
	temp.player.GUID = server->GetGuidFromSystemAddress(sadd);
	temp.player.host = Host;
	temp.player.Name = name;
	InsPlayer(temp);
}

bool CGame::RemPlayer(RakNet::RakNetGUID guid)
{
	RakNet::RakPeerInterface* tool = 0;
	TPlayer find;
	int lookup = FindPlayer(guid);//, find);

	if (lookup >= 0)
	{
		// found player, removing him
		atSnakes.erase(atSnakes.begin()+lookup);
	}

	// player not found
	return false;
}

int CGame::FindPlayer(RakNet::RakNetGUID guid)//, TPlayer& player)
{
	for(int i = 0; i < Size(); ++i)
	{
		if(atSnakes[i].player.GUID == guid)
		{
			//player = atPlayers[i];
			return i;
		}
	}
	return -1;
}

//
//char* CGame::GetSnakeState()
//{
//	std::string Snakeinfo;
//
//
//}