#pragma once
#include <vector>
#include "../Common/Constants/Structs.h"
#include <RakNetTypes.h>
#include "../OphidiaClient/Snake.h"

class CGame
{
public:
	CGame(void);
	~CGame(void);
	void InsPlayer(TSnake& _snake);
	void InsPlayer(RakNet::RakString name, RakNet::SystemAddress sadd, bool Host);
	bool RemPlayer(wchar_t name[256]);
	bool RemPlayer(RakNet::RakNetGUID guid);

	//void InsPlayer(wchar_t name[256], RakNet::SystemAddress sadd);

	void SetState(char _state) { cState = _state; };
	char& GetState() { return cState; };
	// Fetch player list
	const std::vector<TSnake>& GetPlayers() { return atSnakes; };
	const std::vector<char*> GetSnakeStates();
	int FindPlayer(RakNet::RakNetGUID guid);
	// How many players in the game?
	char Size() { return atSnakes.size(); };
	bool isFull() { return Size() < maxSize ? false : true; };

public:
	RakNet::RakPeerInterface* server;

private:

	std::vector<TSnake>		atSnakes;
	char					cState;
	int						maxSize;
};

