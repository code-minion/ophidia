#include <stdio.h>
#include <string.h>
#include "MessageIdentifiers.h"
#include "RakPeerInterface.h"
#include "BitStream.h"
#include "RakNetTypes.h"
#include <conio.h>
#include "RakSleep.h"
#include "Gets.h"
#include "GetTime.h"

#define MAX_CLIENTS 16
#define SERVER_PORT 63005

enum GameMessages
{
	ID_GAME_MESSAGE_1 = ID_USER_PACKET_ENUM + 1,
	ID_ACK = ID_USER_PACKET_ENUM + 2
};

enum GameStates
{
	DEFAULT = -1,
	STANDBY = 0,
	LOBBY_HOSTED = 1,
	GAME_IN_PROGRESS = 2,
	GAME_OVER = 3
};

char cHostState;
RakNet::SystemAddress ipadd;

int main()
{
	// Init Network variables for use
	RakNet::RakPeerInterface *server = RakNet::RakPeerInterface::GetInstance();
	RakNet::Packet *packet;
	RakNet::TimeMS quitTime;
	bool haveIP = false;
	char str[512];
	printf( "Starting the server.\n");
	// Server has to be started to respond to pings
	RakNet::SocketDescriptor sd(SERVER_PORT,0);
	sd.socketFamily = AF_INET;
	bool check = server->Startup(MAX_CLIENTS, &RakNet::SocketDescriptor(), 1) == RakNet::RAKNET_STARTED;
	cHostState = STANDBY;

	server->SetMaximumIncomingConnections(MAX_CLIENTS);

	if (check)
		printf("Server started, waiting for connections.\n");
	else
	{ 
		printf("Server failed to start.  Terminating.\n");
		exit(1);
	}

	printf("How many seconds to run this sample for?\n");
	gets_s(str, sizeof(str));
	if (str[0]==0)
	{
		printf("Defaulting to 5 seconds\n");
		quitTime = RakNet::GetTimeMS() + 5000;
	}
	else
		quitTime = RakNet::GetTimeMS() + atoi(str) * 1000;

	// Loop for input
	while (RakNet::GetTimeMS() < quitTime)
	{
		packet = server->Receive();

		if (packet==0)
		{
			RakSleep(30);
			continue;
		}

		server->DeallocatePacket(packet);

		RakSleep(30);
	}

	while (true)
	{
		for (packet=server->Receive(); packet; server->DeallocatePacket(packet), packet=server->Receive())
		{
			if (!haveIP)
			{
				ipadd = packet->systemAddress;
				haveIP = true;
			}

			switch (packet->data[0])
			{
				case ID_REMOTE_DISCONNECTION_NOTIFICATION:
					printf("Another client has disconnected.\n");
					break;
				case ID_REMOTE_CONNECTION_LOST:
					printf("Another client has lost the connection.\n");
					break;
				case ID_REMOTE_NEW_INCOMING_CONNECTION:
					printf("Another client has connected.\n");
					break;
				case ID_CONNECTION_REQUEST_ACCEPTED:
				{
					printf("Our connection request has been accepted.\n");

					// Use a BitStream to write a custom user message
					// Bitstreams are easier to use than sending casted structures, and handle endian swapping automatically
					RakNet::BitStream bsOut;
					bsOut.Write((RakNet::MessageID)ID_GAME_MESSAGE_1);
					bsOut.Write("Server : Hello World");
					server->Send(&bsOut,HIGH_PRIORITY,RELIABLE_ORDERED,0,packet->systemAddress,false);
				}			
				break;
				case ID_NEW_INCOMING_CONNECTION:
					printf("A connection is incoming.\n");
					break;
				case ID_NO_FREE_INCOMING_CONNECTIONS:
					printf("The server is full.\n");
					break;
				case ID_DISCONNECTION_NOTIFICATION:
					printf("A client has disconnected.\n");
							
					break;
				case ID_CONNECTION_LOST:
					printf("A client lost the connection.\n");
							
					break;
				case ID_GAME_MESSAGE_1:
					{
						RakNet::RakString rs;
						RakNet::BitStream bsIn(packet->data,packet->length,false);
						bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
						bsIn.Read(rs);
						printf("%s\n", rs.C_String());
					}
					break;
				default:
					printf("Message with identifier %i has arrived.\n", packet->data[0]);
					break;
			}			
		}
			
		if (_kbhit())
		{
			gets_s(str);
			RakNet::BitStream bsOut;
			bsOut.Write((RakNet::MessageID)ID_GAME_MESSAGE_1);				
			bsOut.Write(str);
			server->Send(&bsOut,HIGH_PRIORITY,RELIABLE_ORDERED,0,ipadd,false);
		}				
	}

	RakNet::RakPeerInterface::DestroyInstance(server);

	return 0;
}