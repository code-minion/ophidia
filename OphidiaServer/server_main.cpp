// Global Includes
#include <stdio.h>
#include <string>
#include <conio.h>

// Local includes
#include "MessageIdentifiers.h"
#include "RakPeerInterface.h"
#include "BitStream.h"
#include "RakNetTypes.h"
#include "RakSleep.h"
#include "Gets.h"
#include "GetTime.h"
#include "Game.h"
#include "RakString.h"
#include <vector>
#include "../Common/Constants/NetMessage.h"

#define MAX_CLIENTS 17
#define SERVER_PORT 12082

enum Server_States
{
	ST_DEFAULT = -1,
	ST_STANDBY = 0,
	ST_LOBBY = 1,
	ST_GAME_INPROGRESS = 2,
	ST_GAME_OVER = 3
};

char cHostState;
RakNet::RakPeerInterface *server;
RakNet::SystemAddress ipadd;
CGame* GameInstance;

bool ServerStart(RakNet::RakPeerInterface* server);
void ProcessPacket(RakNet::Packet*);
bool StartLobby();
void ProcessLobby();
char PlayerJoin(RakNet::Packet* packet);
RakNet::RakString GetName(RakNet::Packet*);

int main()
{
	// Init/Declare variables for use
	server = RakNet::RakPeerInterface::GetInstance();
	RakNet::Packet *packet;
	bool haveIP = false;
	GameInstance = new CGame();
	GameInstance->SetState(ST_STANDBY);	

	printf( "Starting the server.\n");
	bool running = ServerStart(server);
	char offMsg[] = "KitSnakes";
	GameInstance->server = server;
	server->SetOfflinePingResponse(offMsg,sizeof(offMsg));
	while (running)
	{
		for (packet=server->Receive(); packet; server->DeallocatePacket(packet), packet=server->Receive())
		{
			if (packet == 0) // if we didn't actually receive a packet
			{
				RakSleep(20); // wait a while, then continue
				continue;
			}

			// Process packet messages
			ProcessPacket(packet);
			char state = GameInstance->GetState();

			switch(state)
			{
				case (ST_LOBBY): // if we started a lobby
				{
					// run loop for lobby
					
				}

				case (ST_GAME_INPROGRESS):
				{
					// run game logic loop
				}
				case (ST_GAME_OVER):
				{
					// send GAME OVER to clients
					// Use a BitStream to write a custom user message
					// Bitstreams are easier to use than sending casted structures, and handle endian swapping automatically
					RakNet::BitStream bsOut;
					bsOut.Write((RakNet::MessageID)ID_SV_MESSAGE);
					bsOut.Write("");
					server->Send(&bsOut,HIGH_PRIORITY,RELIABLE_ORDERED,0,packet->systemAddress,false);
				}

			}

		}		
	}

	RakNet::RakPeerInterface::DestroyInstance(server);

	return 0;
}

bool ServerStart(RakNet::RakPeerInterface* server)
{
	RakNet::SocketDescriptor sd(SERVER_PORT,0);
	sd.socketFamily = AF_INET;
	bool check = server->Startup(MAX_CLIENTS, &sd, 1) == RakNet::RAKNET_STARTED;

	server->SetMaximumIncomingConnections(MAX_CLIENTS);

	if (check)
	{
		//cHostState = STANDBY;
		printf("Server started, waiting for connections.\n");
	}
	else
	{ 
		printf("Server failed to start.  Terminating.\n");
		exit(1);
	}
	return true;
}

void ProcessPacket(RakNet::Packet* packet)
{	
	//char str[100];
	RakNet::BitStream bsIn(packet->data, packet->length,false);
	RakNet::BitStream bsOut;

	unsigned char data = packet->data[0];
	switch (data)
	{
		case ID_CL_GAME_JOIN: // client wants to join game
			{
				if (PlayerJoin(packet) == 0 )
				{
					bsIn.IgnoreBytes(1);
					wchar_t name[32];
					bsIn.Read(name);
					bsOut.Write((RakNet::MessageID)ID_SV_INVITE);
					bsOut.Write("welcome");
					server->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
				}
				break;
			}
		case ID_UNCONNECTED_PING:
		case ID_UNCONNECTED_PING_OPEN_CONNECTIONS:
			{
				//// A client is looking for servers!
				//RakNet::TimeMS time;
				//bsIn.IgnoreBytes(1);
				//bsIn.Read(time);
				//packet->systemAddress.ToString(true,str,'|');
				//printf("Pinged by %s, with time %i.\n",str, RakNet::GetTimeMS() - time);
				//
				//bsOut.Write((RakNet::MessageID)ID_SV_ADVERTISE);
				//bsOut.Write("KitsSnakes");
				//bsOut.Write(GameInstance->Size());
				//server->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
			}
			break;
		case ID_REMOTE_DISCONNECTION_NOTIFICATION:
			printf("Another client has disconnected.\n");
			break;
		case ID_REMOTE_CONNECTION_LOST:
			printf("Another client has lost the connection.\n");
			break;
		case ID_REMOTE_NEW_INCOMING_CONNECTION:
			printf("Another client has connected.\n");
			break;
		case ID_CONNECTION_REQUEST_ACCEPTED:
			{
				printf("Our connection request has been accepted.\n");

				// Use a BitStream to write a custom user message
				// Bitstreams are easier to use than sending casted structures, and handle endian swapping automatically
				//RakNet::BitStream bsOut;
				//bsOut.Write((RakNet::MessageID)ID_SV_MESSAGE);
				//bsOut.Write("Server : Hello World");
				//server->Send(&bsOut,HIGH_PRIORITY,RELIABLE_ORDERED,0,packet->systemAddress,false);
			}			
			break;
		case ID_NEW_INCOMING_CONNECTION:
			printf("A connection is incoming.\n");
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			printf("The server is full.\n");
			break;
		case ID_DISCONNECTION_NOTIFICATION:
			printf("A client has disconnected.\n");
							
			break;
		case ID_CONNECTION_LOST:
			printf("A client lost the connection.\n");							
			break;
		case ID_SV_MESSAGE:
			{
				RakNet::RakString rs;
				RakNet::BitStream bsIn(packet->data,packet->length,false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(rs);
				printf("%s\n", rs.C_String());
			}
			break;
		case ID_CL_GAME_LEAVE:
			{
				if (cHostState > 0)
				{
					GameInstance->RemPlayer(packet->guid);
					if(1 > GameInstance->Size())
					{
						// shut lobby down
					}
				}
			}
		default:
			printf("Received message with unexpected identifier : %i.\n", packet->data[0]);
			break;
	}
}

// Return Values :
// 0 = joined
// 1 = full
// 2 = busy
// 3 = already in
char PlayerJoin(RakNet::Packet* packet)
{
	RakNet::BitStream bsOut;
	// check for duplicate user
	if (GameInstance->FindPlayer(packet->guid) == -1)
	{
		// check for state
		if (cHostState < ST_LOBBY)
		{// if lobby hasn't been started yet
			cHostState = ST_LOBBY;
			GameInstance->InsPlayer(GetName(packet),packet->systemAddress, true);		
			return 0;
		}

		if (cHostState == ST_LOBBY)
		{// if we're in the lobby							
			if (!GameInstance->isFull())
			{
				GameInstance->InsPlayer(GetName(packet),packet->systemAddress, false);
				return 0;
			}
			else
			{
				bsOut.Write((RakNet::MessageID)ID_SV_FULL);
				server->Send(&bsOut,LOW_PRIORITY,RELIABLE_ORDERED,0,packet->systemAddress,false);
				return 1;
			}
		}
		else
		{// game in progress
			bsOut.Write((RakNet::MessageID)ID_SV_INPROGRESS);
			bsOut.Write("Sorry, the game has already started!");
			server->Send(&bsOut,LOW_PRIORITY,RELIABLE_ORDERED,0,packet->systemAddress,false);
			return 2;
		}
	}
	else
	{// You're already connected!
		bsOut.Write((RakNet::MessageID)ID_SV_MESSAGE);
		bsOut.Write("You're already connected!");
		server->Send(&bsOut,LOW_PRIORITY,RELIABLE_ORDERED,0,packet->systemAddress,false);
		return 3;
	}
	return -1;
}

RakNet::RakString GetName(RakNet::Packet* pack)
{
	RakNet::BitStream bsIn(pack->data,pack->length,false);
	bsIn.IgnoreBytes(3);
	RakNet::RakString name;
	bsIn.Read(name);
	//memcpy(name,(char*)pack->data[1],sizeof(name));
	printf("User Named : %s\n",name);
	
	return name;
}